var API_ROOT = 'http://it2810-20.idi.ntnu.no/api/';

var get_random_gif = () => {

    var possible_random_images = [
        "http://s.ripost.hu/kepadatbazis/f15dfc7c717e7a9ea763e6899db3d394a29caae9/aa298563f9c7d997af3cb79f84a74f022ed98fb2.gif",
        "https://admin.mashable.com/wp-content/uploads/2013/07/crazy-dance.gif",
        "https://media.giphy.com/media/14urMYvFxIKEms/giphy.gif",
        "http://pjpies.com/wp-content/uploads/2014/11/2zpjxj7.gif",
        "http://www.reactiongifs.com/r/wfa.gif",
        "http://www.reactiongifs.com/r/stoke.gif",
        "http://www.reactiongifs.com/r/msli.gif",
        "https://admin.mashable.com/wp-content/uploads/2013/07/Jeremy-Renner.gif",
        "http://www.reactiongifs.com/r/pnda.gif",
        "http://www.reactiongifs.com/r/cheering_minions.gif",
        "http://www.reactiongifs.com/r/yay2.gif",
        "http://www.reactiongifs.com/r/ysss.gif",
        "http://www.reactiongifs.com/r/prdfudg.gif",
        "http://www.reactiongifs.com/r/chffd.gif",
        "http://www.reactiongifs.com/r/zgtu.gif",
        "http://www.reactiongifs.com/r/yaay.gif",
        "http://www.reactiongifs.com/r/hpp.gif",
        "http://www.reactiongifs.com/r/tyu.gif",
        "http://www.reactiongifs.com/r/vhpy.gif",
        "http://www.reactiongifs.com/r/dink.gif",
        "http://www.reactiongifs.com/r/dafoe.gif",
        "http://www.reactiongifs.com/r/ybrws.gif",
        "http://www.reactiongifs.com/r/yay.gif",
        "http://www.reactiongifs.com/r/zfb.gif",
        "http://www.reactiongifs.com/r/bust.gif",
        "http://www.reactiongifs.com/r/dnc.gif",
        "http://www.reactiongifs.com/r/happy-driving.gif",
        "http://www.reactiongifs.com/r/obama.gif",
        "http://www.reactiongifs.com/wp-content/uploads/2014/01/yippie.gif",
        "http://www.reactiongifs.com/wp-content/uploads/2013/12/yeehaw.gif",
        "http://www.reactiongifs.com/wp-content/uploads/2013/11/stoked.gif",
        "http://www.reactiongifs.com/wp-content/uploads/2013/11/bradshawface.gif"
    ];

    var get_random_int = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    return possible_random_images[get_random_int(0, possible_random_images.length - 1)];

};

var state = {

    selected_category: null,
    front_page_view_is_active: true,
    form_view_is_active: null,

    entities: [{
        text: "When there's more coffee",
        image_url: "http://www.ece.cmu.edu/~ece549/spring15/team03/website/images/pic02.jpg",
        category: "everyday-things"
    }, {
        text: "When the entire project finally works",
        image_url: "http://i.imgur.com/q4gWZ69.gif",
        category: "it-things"
    }, {
        text: "When child mortality drops by yet another 10%",
        image_url: "http://cdn1.globalissues.org/i/children/under5-mortality-rate-1960-2005.png",
        category: "world-things"
    }, {
        text: "When I win at bowling",
        image_url: "https://media.giphy.com/media/Fc6pf86FKCeA/giphy.gif",
        category: "not-so-everyday-things"
    }, {
        text: "When my code runs correctly on the first run",
        image_url: "https://media.giphy.com/media/GA2FNpP1kAQNi/giphy.gif",
        category: "it-things"
    }, {
        text: "When I'm allowed to pop bubblewrap",
        image_url: "https://upload.wikimedia.org/wikipedia/commons/6/6b/Bubble_Wrap.jpg",
        category: "everyday-things"
    }]

};

var generate_form = () => {

    var return_str = `
    <div style="width:100%;margin-top:20px">
      <p class="form-element-description">Description of the thing that makes you happy (maximum 140 characters)</p>

        <div id="form--request-for-missing-input"> </div>'
        <input class="form--input" maxlength="140" id="form--text" type="text" />

        <p class="form-element-description">URL of image or gif (optional)</p>
        <input class="form--input" id="form--image_url" type="text" />

        <p class="form-element-description">Category</p>
        <select class="form--select" id="form--category">
          <option value="everyday-things">Everyday things</option>
          <option value="not-so-everyday-things">Not so everyday things</option>
          <option value="it-things">IT things</option>
          <option value="world-things">World things</option>
          <option value="relational-things">Relational things</option>
          <option value="esthetic-things">Esthetic things</option>
        </select>

      <div class="button" id="button--submit-form">Share your thankfulness</div>

    </div>`;

    $(document).ready(() => {

        $('#button--submit-form').click(() => {
            event__handle_form();
        });

    });

    return return_str;

};

var get_selected_entities = () => {

    if (!state.selected_category) return state.entities;

    return state.entities.filter((x) => {
        return x.category === state.selected_category
    });

};

var generate_div_from_entity = (entity) => {

    var text = entity.text;
    var image_url = entity.image_url;
    var category = entity.category;

    var return_str =
        `<div class="box-container">
        <div class="box-div box--image-div" style="background-image: url(${image_url})"></div>
        <div class="box-div box--text-div">${text}</div>
      </div>`;

    return return_str;

};

var generate_box_list_div = () => {

    var html_representing_entities = get_selected_entities().map(x => generate_div_from_entity(x)).join('');

    var return_str =
        `<div>
          <div id="box-list-container">${html_representing_entities}</div>
          <div class="button" id="button--to-form">What makes <i>you</i> thankful?</div>
          <br />
      </div>`;

    $(document).ready(() => {

        $("#button--to-form").click(() => {

            event__activate_form();

        });

    });

    return return_str;

};

var generate_view = () => {

    var h2_text;
    var form = null;

    if (state.selected_category) {

        if (state.selected_category === 'world-things') {
            h2_text = "World events that make us thankful:";
        }
        if (state.selected_category === 'it-things') {
            h2_text = "IT-related things that make us thankful:";
        }
        if (state.selected_category === 'not-so-everyday-things') {
            h2_text = "Not so everyday things that make us thankful:";
        }
        if (state.selected_category === 'everyday-things') {
            h2_text = "Everyday things that make us thankful:";
        }
        if (state.selected_category === 'esthetic-things') {
            h2_text = "Esthetic things that make us thankful:";
        }
        if (state.selected_category === 'relational-things') {
            h2_text = "Relational things that make us thankful:";
        }

    } else if (state.front_page_view_is_active) {

        h2_text = "Things that make us thankful:";

    } else {

        h2_text = "Tell us about something that makes you thankful:";

        form = generate_form();

    }

    var header = `
      <h2 id="h2-header">
        ${h2_text}
      </h2>`;

    if (!state.form_view_is_active) $("#main-container").replaceWith('<div id="main-container">' + header + generate_box_list_div() + '</div>');
    else $("#main-container").replaceWith('<div id="main-container">' + header + form + '</div>');

};

var activate_individual_entity_view = () => {

};

var event__handle_form = () => {

    var category = $("#form--category").val();
    var image_url = $("#form--image_url").val();
    var text = $("#form--text").val();

    var add_new_entity = true;

    if (!text) {
        add_new_entity = false;
    }

    if (!image_url) {
        image_url = get_random_gif();
    }

    var new_entity = {
        category: category,
        image_url: image_url,
        text: text
    };

    if (add_new_entity) {

        state.entities = [new_entity].concat(state.entities);
        event__activate_home_view();

        $.ajax({
            type: "POST",
            url: API_ROOT + 'entities',
            data: new_entity,
            success: () => console.log("It was posted :)")
        });

    } else {

        $('#form--request-for-missing-input').text('This field is mandatory');

    }

};

var event__activate_home_view = () => {

    state.selected_category = null;
    state.front_page_view_is_active = true;
    state.form_view_is_active = false;

    generate_view();

};

var event__activate_form = () => {

    state.selected_category = null;
    state.front_page_view_is_active = false;
    state.form_view_is_active = true;

    generate_view();

    $("#button").click(() => {

        event__handle_form();

    });

};

var event__activate_category_page = (category) => {

    state.selected_category = category;
    state.front_page_view_is_active = false;
    state.form_view_is_active = false;

    generate_view();

};

$(document).ready(() => {

    generate_view();

    $("#header-element--home").click(() => {

        event__activate_home_view();

    });

    $("#header-element--everyday-things").click(() => {

        event__activate_category_page("everyday-things");

    });

    $("#header-element--not-so-everyday-things").click(() => {

        event__activate_category_page("not-so-everyday-things");

    });

    $("#header-element--it-things").click(() => {

        event__activate_category_page("it-things");

    });

    $("#header-element--world-things").click(() => {

        event__activate_category_page("world-things");

    });

    $("#header-element--esthetic-things").click(() => {

        event__activate_category_page("esthetic-things");

    });

    $("#header-element--relational-things").click(() => {

        event__activate_category_page("relational-things");

    });

    $("#header-element--what-makes-you-thankful").click(() => {

        event__activate_form();

    });

    $.ajax({
        type: 'GET',
        url: API_ROOT + 'entities',
        data: {
            format: 'json'
        },
        error: (err) => {
            console.log(err);
        },
        success: (result) => {
            state.entities = result;
            generate_view();
        },

    });

});
