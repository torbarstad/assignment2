# Group 20, assignment 2

### Usage of implementation

The application allows visitors to share things that makes them thankful.

This is what the front page looks like:

![N|Solid](https://s15.postimg.io/710pr07l5/Screenshot_2016_09_16_21_13_08.png)

If a category from the header is chosen, then only things that have been posted with that category are shown (in the picture below *IT things* has been clicked in the header):

![N|Solid](https://s15.postimg.io/4xqajc7sb/Screenshot_2016_09_16_21_16_10.png)

Visitors that click on the button or the header element named *What makes you thankful?* will be directed to a form where they can share their own thanfulness:

![N|Solid](https://s15.postimg.io/fyq123pez/Screenshot_2016_09_16_21_16_39.png)

They must add text and category, but specifying the URL of the picture or gif they want to use is optional (if no URL is provided then a random gif is chosen from an array of gifs).

Users that try to submit the form without specifying the text field are told that this field is mandatory:

![N|Solid](https://s15.postimg.io/lo69mevl7/Screenshot_2016_09_16_21_16_58.png)

But when they do provide a value for the text field, their submission is added to the frontpage and to the page of the category that was chosen for the submission:

![N|Solid](https://s17.postimg.io/upd84lk4f/Screenshot_2016_09_16_21_36_32.png)

![N|Solid](https://s15.postimg.io/52yn6r4h7/Screenshot_2016_09_16_21_18_32.png)

### Implementation of the application

As instructed we have implemented this webp application in Node.js using the Express.js framework.

We have also used the following NPM packages:

- *body-parser*: This package helped us fetch data from post requests more easily.
- *mongodb*: The database that we used.
- *mongoose*: An ORM that makes it easier to use MongoDB.

Our code is organized as follows:

- *package.json*: As in other Node.js projects, this file specifies the settings of the project (the dependencies, which file is to be called by default in order to run the project, etc).
- *server.js*: This is the file that runs the server.
- *models/entity.js*: This file uses Mongoose to specify the schema/structure of only entity in the application (which we've named *entity*).
- *public/*: The files in this folder (index.html, main.js and styles.css) form the frontend of the application.

To get a better idea of how the application is implemented, let us take a look at some snippets from some of the files in the application and give a brief explanation of how they work.

##### server.js
The contents of the public-folder are made available, and since the html file is called *index*.html it will be made available when visitors ask for *[domainname]/*.

````
app.use(express.static(process.cwd() + '/public'));
````

The schema Entity, which we defined in Mongoose, is imported from the *entity.js*-file in the *models*-folder.

````
var Entity = require('./models/entity.js');
````

An express router is assigned to the variable router.

````
var router = express.Router();
````

A GET-call to test if the API is running, a GET-call for retrieving entities and a POST-call for adding entities are added to the previously mentioned router. To two latter calls rely on the functionality from Mongoose for adding and retrieving entries to and from the database, made available through on the schema *Entity* that was defined using Mongoose.

````
router.get('/', (req, res) => {
    res.json({
        message: 'Welcome to the glorious API of group 20 :)'
    });
});

router.get('/entities', (req, res) => {
    Entity.find((err, entities) => {
        if (err)
            res.send(err);

        res.json(entities);
    });
});

router.post('/entities', (req, res) => {

    var entity = new Entity();
    entity.text = req.body.text;
    entity.image_url = req.body.image_url;
    entity.category = req.body.category;

    entity.save((err) => {
        if (err)
            res.send(err);

        res.json({
            entity
        });
    });

});
````

The previously mentioned router is, by the use of middleware, instructed to be "appended" to the slug *api/*. This makes it so that the possible API-calls are *[domainname]/api* (GET-call) and *[domainname]/entities* (GET-call and POST-call).

##### index.html

The header fetches jQuery as well as our own JavaScript from main.js. jQuery is fetched first, as main.js is dependent on jQuery.

````
<head>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="main.js"></script>
</head>
````

The first content of the body of the html document is the header. With the exeption of inline styling that is used to specify the dimentions of the home-icon, the CSS of the header is specified in styles.css (using flexbox).
````
<nav id="header">
        <div class="header-element" id="header-element--home">
          <img style="height:30px;width:30px;" src="http://bsccongress.com/im15/gray-home-icon-clip-art.png"/>
        </div>
        <div class="header-element" id="header-element--everyday-things"><p>Everyday things</p></div>
        <div class="header-element" id="header-element--not-so-everyday-things"><p>Not so everyday things</p></div>
        <div class="header-element" id="header-element--it-things"><p>IT things</p></div>
        <div class="header-element" id="header-element--world-things"><p>World things</p></div>
        <div class="header-element" id="header-element--what-makes-you-thankful"><p>What makes <i>you</i> thankful?</p></div>
    </nav>
````

Beyond the header, the only html in index.html is the div bellow. The content of this div is meant to be filled using jQuery, so as one might intuitively assume, something has not worked as intended if the text *Oops* is all that is seen below the header.

````
<div id="main-container">
    Oops
</div>
````

##### main.js

The state of the website is stored in an object named *state*.

The booleans *selected_category*, *front_page_view_is_active* and *form_view_is_active* suffice to deduce whether the front page is active or the form is active or whether the user has navigated to any of the specific categories in the navnbar.

The array entities contain the content of the boxes that are shown on the website. There are some that are added by default, but if the GET-query to the server asking for entities is succesful (more on this query later in) the default content of the array will be replaced.

````
var state = {

    selected_category: null,
    front_page_view_is_active: true,
    form_view_is_active: null,

    entities: [{
        text: "When there's more coffee",
        image_url: "http://www.ece.cmu.edu/~ece549/spring15/team03/website/images/pic02.jpg",
        category: "everyday-things"
    }, {
        text: "When the entire project finally works",
        image_url: "http://i.imgur.com/q4gWZ69.gif",
        category: "it-things"
    }, {
        text: "When child mortality drops by yet another 10%",
        image_url: "http://cdn1.globalissues.org/i/children/under5-mortality-rate-1960-2005.png",
        category: "world-things"
    }, {
        text: "When I win in bowling",
        image_url: "https://media.giphy.com/media/Fc6pf86FKCeA/giphy.gif",
        category: "not-so-everyday-things"
    }, {
        text: "When my code runs correctly on the first run",
        image_url: "https://media.giphy.com/media/GA2FNpP1kAQNi/giphy.gif",
        category: "it-things"
    }, {
        text: "When I'm allowed to pop bubblewrap",
        image_url: "https://upload.wikimedia.org/wikipedia/commons/6/6b/Bubble_Wrap.jpg",
        category: "everyday-things"
    }]

};
````

The function *generate_view* generates a string with the html of the content that is shown below the navbar. It makes use of other functions that also generate strings of html, namely *generate_form* and *generate_box_list_div*. Also, as can be seen, the specific html that is generated depends on the data that is stored in the previosuly mentioned state object. Once the string of html has been generated it replaces the current content of the div that is below the navbar (through the use of jQuery).

````
var generate_view = () => {

    var h2_text;
    var form = null;

    if (state.selected_category) {

        if (state.selected_category === 'world-things') {
            h2_text = "World events that make us thankful:";
        }
        if (state.selected_category === 'it-things') {
            h2_text = "IT-related things that make us thankful:";
        }
        if (state.selected_category === 'not-so-everyday-things') {
            h2_text = "Not so everyday things that make us thankful:";
        }
        if (state.selected_category === 'everyday-things') {
            h2_text = "Everyday things that make us thankful:";
        }

    } else if (state.front_page_view_is_active) {

        h2_text = "Things that make us thankful:";

    } else {

        h2_text = "Tell us about something that makes you thankful:";

        form = generate_form();

    }

    var header = `
      <h2 id="h2-header">
        ${h2_text}
      </h2>`;

    if (!state.form_view_is_active) $("#main-container").replaceWith('<div id="main-container">' + header + generate_box_list_div() + '</div>');
    else $("#main-container").replaceWith('<div id="main-container">' + header + form + '</div>');

};
````

At first (once the DOM is ready for javascript-code to run, as detected by the jQuery *ready*-function) the view is generated using the previously mentioned *generate_view*-function.

Then, as specified in the anonymious function that is provided to the *ready*-function as parameter:

- Specific functions are ordered to run once different header-elements (as identified and selected through their *ids*) are clicked. These functions update the state and then call on *generate_view* to run again. This is done with the help of *jQuery even bindings*, which enables us to easily specify which functions that are to run once specific actions happens to specific elements in the DOM that are specified with the help of *jQuery selectors*.
- An ajax GET-call to fetch entities from the backend is made. If successful these entities are added to the state object and the *generate_view*-function is thereafter called once more (as specified in the anonymious function that is added to the *success*-parameter of the settings-object of the ajax-call).

````
$(document).ready(() => {

    generate_view();

    $("#header-element--home").click(() => {

        event__activate_home_view();

    });

    $("#header-element--everyday-things").click(() => {

        event__activate_category_page("everyday-things");

    });

    $("#header-element--not-so-everyday-things").click(() => {

        event__activate_category_page("not-so-everyday-things");

    });

    $("#header-element--it-things").click(() => {

        event__activate_category_page("it-things");

    });

    $("#header-element--world-things").click(() => {

        event__activate_category_page("world-things");

    });

    $("#header-element--what-makes-you-thankful").click(() => {

        event__activate_form();

    });

    $.ajax({
        type: 'GET',
        url: API_ROOT + 'entities',
        data: {
            format: 'json'
        },
        error: (err) => {
            console.log(err);
        },
        success: (result) => {
            state.entities = result;
            generate_view();
        },

    });

});
````

Among the different functions that handle events, *event__handle_form*, which is called when the submit-form-button is clicked, seems most worthy of specific explanation. Using jQuery selectors it finds the form elements and using the jQuery-function *val* it extracts their values.

If the input-field where the text of the box is to be specified is empty, the data from the form is not posted, and instead a text saying *This field is mandatory* is added to the initially empty div where this message is to be displayed.

If no image or gif is provided a random one is selected with help from our function *get_random_gif*.

When there is no error and the content is to be posted, an AJAX-POST-request is made to the server. As an example of one of the many differences between choices made for this application and choices that would have been made for a real-world application, the *state* is updated whether or not the request to the backend is succesful, and not changed in any way if an error occurs (visitors will however notice the difference between whether or not communication with the server is working if they refresh the page).

````
var event__handle_form = () => {

    var category = $("#form--category").val();
    var image_url = $("#form--image_url").val();
    var text = $("#form--text").val();

    var add_new_entity = true;

    if (!text) {
        add_new_entity = false;
    }

    if (!image_url) {
        image_url = get_random_gif();
    }

    var new_entity = {
        category: category,
        image_url: image_url,
        text: text
    };

    if (add_new_entity) {

        state.entities = [new_entity].concat(state.entities);
        event__activate_home_view();

        $.ajax({
            type: "POST",
            url: API_ROOT + 'entities',
            data: new_entity,
            success: () => console.log("It was posted :)")
        });

    } else {

        $('#form--request-for-missing-input').text('This field is mandatory');

    }

};
````

##### styles.css

No css libraries are used anywhere.

Flexbox is used in the styling of the navbar.

````
#header {
    display: flex;
    ...
}
````

````
#header-element {
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    ...
}
````

Flexbox is also used in the styling of the boxes.
