var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EntitySchema = new Schema({
    text: String,
    image_url: String,
    category: String
});

module.exports = mongoose.model('Entity', EntitySchema);
