var express = require('express');
var app = express();
var body_parser = require('body-parser');
var mongoose = require('mongoose');
var router = express.Router();

mongoose.connect('mongodb://localhost:27017/test');

var Entity = require('./models/entity.js');

var port = process.env.PORT || 80;

app.use(body_parser.json());

app.use(body_parser.urlencoded({
    extended: true
}));

app.use(express.static(process.cwd() + '/public'));

router.get('/', (req, res) => {
    res.json({
        message: 'Welcome to the glorious API of group 20 :)'
    });
});

router.get('/entities', (req, res) => {
    Entity.find((err, entities) => {
        if (err)
            res.send(err);

        res.json(entities);
    });
});

router.post('/entities', (req, res) => {

    var entity = new Entity();
    entity.text = req.body.text;
    entity.image_url = req.body.image_url;
    entity.category = req.body.category;

    entity.save((err) => {
        if (err)
            res.send(err);

        res.json({
            entity
        });
    });

});

app.use('/api', router);

app.listen(port);

console.log('Magic happens on port ' + port);
